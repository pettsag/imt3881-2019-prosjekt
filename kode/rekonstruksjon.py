import cv2
import numpy as np

Zmax = 255
Zmin = 0
n = 256
l = 100

def w(z):
    """
    Beskrivelse
    -----------
    Bestemmer hvor mye en pikselverdi skal vektes.
    Vekten blir høyest for verdier nærme midtverdi og lavest ved min- og maksverdi.
    
    Parameter
    ---------
    z: Pikselverdi (0-255, uint)
    
    Returnerer
    ----------
    Pikselverdiens vekt (uint)
    """
    return(z - Zmin if (z <= .5 *Zmax) else Zmax - z)

def wArray(z):
    """
    Beskrivelse
    -----------
    Bestemmer hvor mye pikselverdiene skal vektes.
    Vekten blir høyest for verdier nærme midtverdi og lavest ved min- og maksverdi.
    
    Parameter
    ---------
    z: Array med pikselverdier(0-255, numpy-array uint)
    
    Returnerer
    ----------
    Array med pikselverdienes vekt (numpy-array uint)
    """
    z[z>.5*Zmax] = Zmax - z[z>.5*Zmax]
    return(z)    

def lesBilderOgLukkertider(bildeListe):
    """
    Beskrivelse
    -----------
    Leser inn og returnerer bilder og deres tilhørende lukkertid
    Lukkertid er de fem siste sifrene i filnavnet(før filformat)
        Eksempel filnavn: mittbilde01024.png
    
    Parameter
    ---------
    bildeListe: Liste med full path til hvert bilde (list med strings)
        Eksempel bildeListe: D:/Directory/Pictures/*.png
        Eksempel bilde:      D:/Directory/Pictures/mittbilde01024.png
    
    Returnerer
    ----------
    Bilder      (numpy-array uint)
    Lukkertider (numpy-array float)
    """
    bilder = [cv2.imread(bilde) for bilde in bildeListe] #cv2 for at opplinjeringen under skal fungere

    alignMTB = cv2.createAlignMTB()     #Opplinjering
    alignMTB.process(bilder, bilder)
    
    bilder = np.array(bilder)           #Cast til np-array

    if(np.array_equal(bilder[0,:,:,0], bilder[0,:,:,1])): #Like kanaler => Graatone-bilde:
        bilder = bilder[:,:,:,0]                             #Hent ut én kanal
    else:                                                 #Fargebilde:
        bilder = bilder[:,:,:,::-1]                          #BGR -> RGB
    
    B = np.zeros(len(bildeListe))   
    for j in range(len(bildeListe)):  #Les eksponeringstider:
        B[j] = bildeListe[j][-9:-4]
    
    return bilder, B

def lagResponsKurve(bilder, B):
    """
    Beskrivelse
    -----------
    Beregner og lager bildenes responskurve
    
    Parametere
    ----------
    bilder: Bilder                (numpy-array uint, 3D(grå)/4D(rgb))
    B     : Bildenes ln lukkertid (numpy-array float)
    
    Returnerer
    ----------
    Responskurven til bildene (numpy-array float)
    """
    
    #Nedskaler bildene(flat ut, velg hver 1000. piksel):
    mindrebilder = bilder.reshape((np.size(bilder, 0), np.size(bilder, 1) * np.size(bilder, 2)))
    mindrebilder = mindrebilder[:,::1000]
    
    A = np.zeros(shape=(np.size(mindrebilder, 0) * np.size(mindrebilder, 1) + n-1, n + np.size(mindrebilder, 1)))
    b = np.zeros(shape=(np.size(A, 0), 1))

    k = 0
    for i in range(len(mindrebilder[0])):
        for j in range(len(mindrebilder)):
            wij = w(mindrebilder[j][i])
            A[k, mindrebilder[j][i]] = wij
            A[k, n+i] = -wij
            b[k] = wij * B[j]
            k += 1

    A[k, 128] = 1
    k += 1

    for i in range(1, n-1):
        A[k, i-1] = l * w(i)
        A[k, i] = -2 * l * w(i)
        A[k, i+1] = l * w(i)
        k += 1

    x = np.linalg.lstsq(A, b, rcond=None)[0]
    g = x[:n]

    return(g)

def lagRadianceMap(bilder, g, B):
    """
    Beskrivelse
    -----------
    Lager bildenes radiance-map
    
    Parametere
    ----------
    bilder: Bilder                (numpy-array uint, 3D(grå)/4D(rgb))
    g:      Bildenes responskurve (numpy-array float)
    B:      Bildenes ln lukkertid (numpy-array float)
    
    Returnerer
    ----------
    Radiance-map til bildene (numpy-array float)
    """
    Z = bilder.reshape(np.size(bilder,0), (np.size(bilder, 1) * np.size(bilder, 2)))
   
    lnE = np.zeros(np.size(bilder, 1) * np.size(bilder, 2))
    teller = np.zeros(np.size(bilder, 1) * np.size(bilder, 2))
    nevner = np.zeros(np.size(bilder, 1) * np.size(bilder, 2))

    for bilde, pikselverdier in enumerate(Z):
        vekt = wArray(pikselverdier) + 0.001             #Ikke del paa 0
        teller += vekt * (np.reshape(g[pikselverdier], (len(teller))) - B[bilde])
        nevner += vekt
    lnE = teller/nevner

    E = np.exp(lnE)
    #Sett range 0-255:
    E += np.abs(min(E))
    E = E/max(E)
    E = E * 255

    return E.reshape(bilder[0].shape)

def rekonstruerHDR(bildeListe):
    """
    Beskrivelse
    -----------
    Konstruerer HDR-bilde gjennom mange bilder av samme motiv med ulik lukkertid.
   
    Parameter
    ---------
    bildeListe: Liste med full path til hvert bilde (list med strings)
        Eksempel bildeListe: D:/Directory/Pictures/*.png
        Eksempel bilde:      D:/Directory/Pictures/mittbilde01024.png
        
    Returnerer
    ----------
    HDR-bilde (numpy-array float, 2D(grå)/3D(RGB))
    """
    bilder, B = lesBilderOgLukkertider(bildeListe)
    B = np.log(B)
    
    hdr = np.zeros((bilder[0].shape))

    if(bilder[0].ndim == 3):    #Fargebilde:
        for i in range(3):          #For hver kanal:
            g = lagResponsKurve(bilder[:,:,:,i], B)
            hdr[:,:,i] = lagRadianceMap(bilder[:,:,:,i], g, B)
    else:                       #Gråtone-bilde:
        g = lagResponsKurve(bilder, B)
        hdr = lagRadianceMap(bilder, g, B)

    return hdr