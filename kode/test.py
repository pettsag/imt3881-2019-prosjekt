import glob
import imageio
import numpy as np
import rekonstruksjon
import rendring
import unittest

pikselverdier = np.array(range(256))
bildeListe = glob.glob('../eksempelbilder/Tree/*.png')

bilder, B = rekonstruksjon.lesBilderOgLukkertider(bildeListe)
g = rekonstruksjon.lagResponsKurve(bilder[:,:,:,0], np.log(B))
E = rekonstruksjon.lagRadianceMap(bilder[:,:,:,0], g, B)
hdr = rekonstruksjon.rekonstruerHDR(bildeListe)

path='../eksempelbilder/Ocean/Ocean.exr'
exrTest = imageio.imread(path, format='EXR-FI')


class test_modul(unittest.TestCase):
        
    def test_log(self):
        self.assertGreaterEqual(rendring.log(exrTest).all(), 0)
        self.assertLessEqual(rendring.log(exrTest).all(), 1)
        
    def test_gamma(self):
        self.assertGreaterEqual(rendring.gamma(exrTest).all(), 0)
        self.assertLessEqual(rendring.gamma(exrTest).all(), 1)

    def test_lumkrom(self):
        self.assertEqual(exrTest.ndim, 3)

    def test_linspat(self):
        self.assertEqual(exrTest.ndim, 3)

    def test_ikkelineaer(self):
        self.assertEqual(exrTest.ndim, 3)        

    def test_w(self):
        self.assertTrue(rekonstruksjon.w(0) == 0)
        self.assertTrue(rekonstruksjon.w(255) == 0)
        self.assertTrue(rekonstruksjon.w(1) == 1)
        self.assertTrue(rekonstruksjon.w(254) == 1)
        self.assertTrue(rekonstruksjon.w(127) == 127)
        self.assertTrue(rekonstruksjon.w(128) == 127)
        self.assertTrue(rekonstruksjon.w(150) == 105)
    
    def test_wArray(self):
        self.assertTrue(rekonstruksjon.wArray(pikselverdier)[0] == 0)
        self.assertTrue(rekonstruksjon.wArray(pikselverdier)[255] == 0)
        self.assertTrue(rekonstruksjon.wArray(pikselverdier)[1] == 1)
        self.assertTrue(rekonstruksjon.wArray(pikselverdier)[254] == 1)
        self.assertTrue(rekonstruksjon.wArray(pikselverdier)[127] == 127)
        self.assertTrue(rekonstruksjon.wArray(pikselverdier)[128] == 127)
        self.assertTrue(rekonstruksjon.wArray(pikselverdier)[150] == 105)
    
    def test_lesBilderOgLukkertider(self):
        self.assertEqual(bilder.ndim, 4)
        self.assertEqual(bilder.shape, (10, 906, 928, 3))
        self.assertEqual(len(B), 10)
        for i in B:
            self.assertFalse(i == 0)
        
    def test_lagResponsKurve(self):
        self.assertEqual(len(g), 256)
        
    def test_lagRadianceMap(self):
        self.assertEqual(E.ndim, 2)
        self.assertEqual(E.shape, (906, 928))
    
    def test_konstruerHDR(self):
        self.assertEqual(hdr.ndim, 3)
        self.assertEqual(hdr.shape, (906, 928, 3))   
