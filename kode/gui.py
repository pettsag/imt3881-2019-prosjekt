import cv2
import glob
import imageio
import numpy as np
import numpy.core.multiarray
import PIL.Image, PIL.ImageTk
import rekonstruksjon
import rendring
from tkinter import *
from tkinter import filedialog

class GUI:    
    def __init__(self, master):
        global alleredeBildeVenstre, vh, vb     # VenstreHoyde, VenstreBredde
        global alleredeBildeHoyre, hh, hb       # HoyreHoyde, HoyreHoyde
        
        alleredeBildeVenstre = 0
        vh = 0
        vb = 0
        alleredeBildeHoyre = 0
        hh = 0
        hb = 0
        
        # Lager hoveddelene
        top_frame = Frame(root, bg="#ECECEC", padx=3, pady=3)
        midt = Frame(root, bg="#ECECEC", padx=3, pady=3)
        bunn_frame = Frame(root, bg="#ECECEC", padx=3, pady=3)

        # Oppsett av hoveddeler
            # Rescaling-regler
        root.grid_rowconfigure(1, weight=1)
        root.grid_columnconfigure(0, weight=1)
            # Områder
        top_frame.grid(row=0, sticky="ew")
        midt.grid(row=1, sticky="nsew")
        bunn_frame.grid(row=2, sticky="ew")
        
        
        #------------------------------------------------------------------------------------------------------------------------------------------------------
        # Lager widgets i top_frame
            # Rescaling regler
        top_frame.grid_columnconfigure(0, weight=1)    # For å få top_venstre til å rescale seg i "column"-retning OG for at sticky skal funke
        top_frame.grid_columnconfigure(2, weight=1)    # For å få top_hoyre til å rescale seg i "column"-retning
        
            # Områder
        top_venstre = Frame(top_frame, bg="#ECECEC", width=490, height=55, padx=3, pady=3)
        top_midt = Frame(top_frame, bg="#ECECEC", width=20, height=55, padx=3, pady=3)
        top_hoyre = Frame(top_frame, bg="#ECECEC", width=490, height=55, padx=3, pady=3)
        
        top_hoyre.grid_propagate(0)     # Setter faste vindustorrelser
        top_venstre.grid_propagate(0)
        
        top_hoyre.grid_columnconfigure(0, weight=1)
        top_hoyre.grid_columnconfigure(1, weight=1)
        top_hoyre.grid_columnconfigure(2, weight=1)
        
        top_venstre.grid_columnconfigure(1, weight=1) 
        
        top_hoyre.grid_rowconfigure(0, weight=1)
        top_hoyre.grid_rowconfigure(1, weight=1)
        top_venstre.grid_rowconfigure(0, weight=1)       
        top_venstre.grid_rowconfigure(1, weight=1)       
        
            # Widgets
        rekonstruerText = Label(top_venstre, text="Rekonstruer HDR", bg="#ECECEC")
        rekonstruerMappe_button = Button(top_venstre, text="Velg mappe", command = self.rekonstruer)
        HDRrendring = Label(top_hoyre, text="Global HDR-rendring", bg="#ECECEC")
        HDRrendring_button = Button(top_hoyre, text="Velg exr-fil", command = lambda : self.rendring(0))    # lambda fordi knappen kaller en funksjon med parameter
        exit_button = Button(top_hoyre, text = "Avslutt", bg = "#EF4C4C", command = root.destroy)
        
        # Oppsett i top_frame
            # Områder
        top_venstre.grid(row=0, column=0, sticky="nsew")            # Fester kantene på midt_venstre
        top_midt.grid(row=0, column=1, sticky="ns")                    # Fester midt_midt til nord og sor men ikke vest og ost for å beholde storrelse
        top_hoyre.grid(row=0, column=2, sticky="nsew")              # Fester kantene på midt_hoyre 
        
            # Widgets
        rekonstruerText.grid(row=0, column=1, sticky="n")           # .grid() for å få vist frem widgets og for å si hvor de skal i sin respektive Frame
        rekonstruerMappe_button.grid(row=1, column=1, sticky="s")
        HDRrendring.grid(row=0, column=1, sticky="n")
        HDRrendring_button.grid(row=1, column=1, sticky="s")
        exit_button.grid(row=0, column=2, sticky="ne")
        
        
        #------------------------------------------------------------------------------------------------------------------------------------------------------
        # Lager midt-widgets
            # Rescaling-regler
        midt.grid_rowconfigure(0, weight=1)       # For å få hele midten til å rescale seg i "row"-retning
        midt.grid_columnconfigure(0, weight=1)    # For å få midt_venstre til å rescale seg i "column"-retning
        midt.grid_columnconfigure(2, weight=1)    # For å få midt_hoyre til å rescale seg i "column"-retning
        
            # Områder
        self.midt_venstre = Frame(midt, bg="#ECECEC", height=430)
        midt_venstre_top = Frame(self.midt_venstre, bg="#ECECEC", height=27, width=430)
        midt_midt = Frame(midt, bg="#ECECEC", height=430, width=20, padx=3, pady=3)
        self.midt_hoyre = Frame(midt, bg="#ECECEC", height=430, padx=3, pady=3)
        
        self.midt_hoyre.grid_propagate(0)     # Setter faste vindusstorrelser
        self.midt_venstre.grid_propagate(0)
        
        self.midt_venstre.grid_columnconfigure(0, weight=1)
        self.midt_venstre.grid_columnconfigure(1, weight=1)
        self.midt_venstre.grid_columnconfigure(2, weight=1)
        
        self.midt_venstre.grid_rowconfigure(0, weight=1)
        self.midt_venstre.grid_rowconfigure(1, weight=1)
        self.midt_venstre.grid_rowconfigure(2, weight=1)
        
        
        self.midt_hoyre.grid_columnconfigure(0, weight=1)
        self.midt_hoyre.grid_columnconfigure(1, weight=1)
        self.midt_hoyre.grid_columnconfigure(2, weight=1)
        self.midt_hoyre.grid_columnconfigure(3, weight=1)
        self.midt_hoyre.grid_columnconfigure(4, weight=1)
        
        self.midt_hoyre.grid_rowconfigure(0, weight=1)
        self.midt_hoyre.grid_rowconfigure(1, weight=1)
        self.midt_hoyre.grid_rowconfigure(2, weight=1)
        
            # Widgets
        HoyreVenstre_Opplinjering = Label(self.midt_venstre, text=" ", bg="#ECECEC")
        
        # Oppsett i midt
            # Områder
        self.midt_venstre.grid(row=0, column=0, sticky="nsew")  # Fester kantene på midt_venstre
        midt_venstre_top.grid(row=0, column=0, sticky="nsew", columnspan=3)
        midt_midt.grid(row=0, column=1, sticky="ns")            # Fester midt_midt til nord og sor men ikke vest og ost for å beholde storrelse
        self.midt_hoyre.grid(row=0, column=2, sticky="nsew")    # Fester kantene på midt_hoyre 
        
            # Widgets
        HoyreVenstre_Opplinjering.grid(row=0, column=1, sticky="n")
        
        #------------------------------------------------------------------------------------------------------------------------------------------------------
        # Lager widgets i bunn_frame
            # Rescaling regler
        bunn_frame.grid_rowconfigure(0, weight=1)
        bunn_frame.grid_columnconfigure(1, weight=1)
        
            # Områder
        bunn_hoyre = Frame(bunn_frame, bg="#ECECEC", height=20, padx=3)
        
            # Widgets
        self.errormelding = Label(bunn_hoyre, bg="#ECECEC")
        self.errormelding.after(0, self.fjern_error_melding)
        
        
        # Oppsett i bunn_frame
            # Områder
        bunn_hoyre.grid(row=0, column=1, sticky="se")
        
            # Widgets
        self.errormelding.grid(row=0, column=1, sticky="se")
        
        
		
    def rekonstruer(self):
        global alleredeBildeVenstre, vb, vh
        dir = str(filedialog.askdirectory())
        
        if(len(dir)):                       # Hvis directory er valgt
            if(alleredeBildeVenstre == 1):  # og det finnes et bilde i vinduet sin venstreside
                self.HDRbilde.grid_remove() # så fjerner vi alt som har med det å gjøre
                self.HDRrender_button.grid_remove()
                self.HDRlagre_button.grid_remove()
                self.HDRavbryt_button.grid_remove()
            
            bildeListe = "".join(dir + "/*")   # Legger alle bilder inn i en liste
            bildeListe = glob.glob(bildeListe)
            
            for i in range(len(bildeListe)):   # For aa faa bort .exr saa programmet fungerer for .png, .jpg osv. Trengs kun aa bli gjort i dette prosjektet, med mappeoppsett som ble gitt
                if(bildeListe[i][-4::] == ".exr"):
                    faaBort = i
            bildeListe.pop(faaBort)
                        
            hdr = rekonstruksjon.rekonstruerHDR(bildeListe)
            
            vb, vh = self.nedskaler(hdr) # For aa nedskalere bildet
            bilde = cv2.resize(hdr, (int(vb), int(vh)))
            self.vinduStorrelse()
            
            displayBilde = PIL.ImageTk.PhotoImage(PIL.Image.fromarray(np.uint8(bilde))) # Gjør om bildet til et PIL.ImageTk for håndtering i Tkinter
            
            self.HDRbilde = Label(self.midt_venstre, image=displayBilde)
            self.HDRbilde.image = displayBilde
            self.HDRbilde.grid(row=1, column=0, columnspan=4, sticky="n")
            alleredeBildeVenstre = 1
            
            self.HDRrender_button = Button(self.midt_venstre, text="Render", command = lambda : self.rendring(hdr))
            self.HDRrender_button.grid(row=2, column=0, padx=10, sticky="ne")
            
            self.HDRlagre_button = Button(self.midt_venstre, text="Lagre", command = lambda : self.lagreBilde(hdr))
            self.HDRlagre_button.grid(row=2, column=1, padx=10, sticky="n")
            
            self.HDRavbryt_button = Button(self.midt_venstre, text="Avbryt", command = self.avbrytHDR)
            self.HDRavbryt_button.grid(row=2, column=2, padx=10, sticky="nw")           
        else:                                       # Ingen directory valgt
            self.errormelding.config(text = "Ingen mappe valgt")
            self.errormelding.grid()
            self.errormelding.after(5000, self.fjern_error_melding)
    
    def rendring(self, hdr):
        global alleredeBildeHoyre
        
        if(alleredeBildeHoyre == 1):    # Om det finnes et bilde i vinduet sin høyreside
            self.avbrytRender()           # så fjern alt som har med det å gjøre
        
        if(isinstance(hdr, int)):     #Bilde maa leses inn:
            dir = str(filedialog.askopenfilename())
                   
            if(len(dir)):                            # Hvis directory valgt
                if(dir[-4:] == '.exr'):
                    hdr = numpy.array(imageio.imread(dir, format='EXR-FI'))
                else:
                    self.errormelding.config(text = "Filformat må være .exr")
                    self.errormelding.grid()
                    self.errormelding.after(5000, self.fjern_error_melding)
                    return 0                              #Avbryt
            else:                                     # Ingen fil valgt
                self.errormelding.config(text = "Ingen fil valgt")
                self.errormelding.grid()
                self.errormelding.after(5000, self.fjern_error_melding)
                return 0                                  #Avbryt
        
        self.var = IntVar() # Må bli gjort for radioButtons

        self.RB1log = Radiobutton(self.midt_hoyre, text="log", variable = self.var, value=1, command = lambda : self.renderlog(hdr))
        self.RB2gamma = Radiobutton(self.midt_hoyre, text="gamma", variable = self.var, value=2, command = lambda : self.rendergamma(hdr))
        self.RB3lumkrom = Radiobutton(self.midt_hoyre, text="lumkrom", variable = self.var, value=3, command = lambda : self.renderlumkrom(hdr))
        self.RB4linspat = Radiobutton(self.midt_hoyre, text="linspat", variable = self.var, value=4, command = lambda : self.renderlinspat(hdr))
        self.RB5ikkelineaer = Radiobutton(self.midt_hoyre, text="ikkelineær", variable = self.var, value=5, command = lambda : self.renderikkelineaer(hdr.astype(np.float32)))
        
        self.RB1log.grid(row=0, column=0, sticky="n")
        self.RB2gamma.grid(row=0, column=1, sticky="n")
        self.RB3lumkrom.grid(row=0, column=2, sticky="n")
        self.RB4linspat.grid(row=0, column=3, sticky="n")
        self.RB5ikkelineaer.grid(row=0, column=4, sticky="n")
        
    def visRender(self, renderBilde):
        global alleredeBildeHoyre, hb, hh, lagreRenderBilde 
        lagreRenderBilde = renderBilde      # For aa oppdatere renderbildet som vi skal lagre
        
        if(alleredeBildeHoyre == 1):
            self.R_bilde.grid_remove()
            alleredeBildeHoyre = 0
        else:
            self.R_lagre_button = Button(self.midt_hoyre, text="Lagre", command = lambda : self.lagreBilde(lagreRenderBilde))
            self.R_lagre_button.grid(row=2, column=1, columnspan=2, padx=10, sticky="n")
            
            self.R_avbryt_button = Button(self.midt_hoyre, text="Avbryt", command = self.avbrytRender)
            self.R_avbryt_button.grid(row=2, column=2, columnspan=2, padx=10, sticky="n")
        
        bilde = renderBilde * 255
        
        hb, hh = self.nedskaler(bilde)        # For aa nedskalere bildet
        bilde = cv2.resize(bilde, (int(hb), int(hh)))
        self.vinduStorrelse()                 # For aa finne riktig/ny vinduStorrelse
        
        displayBilde = PIL.ImageTk.PhotoImage(PIL.Image.fromarray(np.uint8(bilde)))
        
        self.R_bilde = Label(self.midt_hoyre, image=displayBilde)
        self.R_bilde.image = displayBilde
        self.R_bilde.grid(row=1, column=0, columnspan=5, sticky="n")
        alleredeBildeHoyre = 1
    
    def nedskaler(self, bilde):
        bredde = np.size(bilde, 1)
        hoyde = np.size(bilde, 0)
        
        while(bredde > 900 or hoyde > 850):         # Hvis bildet er for stort
            skalering = 0.95                                     # nedskaler 5% og prøv igjen
            bredde = bredde * skalering
            hoyde = hoyde * skalering

        return(bredde, hoyde)
        
    def vinduStorrelse(self):
        global vh, vb, hh, hb
        vinduStorrelse = root.winfo_geometry()                  # Finner naavaerende vindustørrelse, som blir seende slik ut: "1000x500+200+200"
        vinduBredde = int(vinduStorrelse.split('x')[0])                # splitter saa ved 'x' for aa faa ut bredde 
        vinduHoyde = int(vinduStorrelse.split('x')[1].split('+')[0]) # Splitter saa paa nytt ved 'x' ogsaa igjen paa '+' for aa faa tak i hoyde
        
            # Oppskalering av vinduet i forhold til bildene allerede i vinduet
        if(vinduBredde/2 - 20 < vb):       # Venstrebildets bredde blir for stor:
            vinduBredde = vb * 2 + 20       # *2 fordi vi har delt vindu i 2, + 20 for midtdeler
        elif(vinduBredde/2 - 20 < hb):    # Hoyrebildets bredde blir for stor:
            vinduBredde = hb * 2 + 20
        
        if(vinduHoyde - 55 - 20 - 60 < vh):
            vinduHoyde = vh + 55 + 20 + 60 + 20 # Hoyde skal vaere bilde-hoyde + top_frame hoyde + bunn_frame hoyde + knappene under bildet + buffer og rb over høyre bilde
        elif(vinduHoyde - 55 - 20 - 60 < hh):
            vinduHoyde = hh + 55 + 20 + 60 + 20
        
            # Nedskalering av vinduet i forhold til bildene allerede i vinduet
                # Venstrebildet er bredere enn hoyrebilde & vindubredde er for stor
        if(vb >= hb and vinduBredde >= vb + 55 + 20 + 60):
            vinduBredde = vb * 2 + 20
                # Hoyrebildet er bredere enn venstrebilde & vindubredde er for stor
        elif(hb >= vb and vinduBredde >= hb + 55 + 20 + 60):
            vinduBredde = hb * 2 + 20
            
                # Er venstrebildet hoyere enn hoyre og mindre enn vinduet
        if(vh >= hh and vinduHoyde >= vh + 55 + 20 + 60):
            vinduHoyde = vh + 55 + 20 + 60 + 20 # Nedskaler til venstre sin hoyde
                # Er hoyrebildet hoyere enn venstre og mindre enn vinduet
        elif(hh >= vh and vinduHoyde >= hh):
            vinduHoyde = hh + 55 + 20 + 60 + 20 # Nedskaler til hoyre sin hoyde
        
        root.geometry("%dx%d+0+0" % (vinduBredde, vinduHoyde))    # Sett vindu til riktig storrelse, og i pksel (x, y) (fra topvenstre hjorne)

    def renderlog(self, exr):       # Dette er de forskjellige render-funksjonene
        exrlog = rendring.log(exr)
        self.visRender(exrlog)
        lagreRenderBilde = exrlog

    def rendergamma(self, exr):
        exrgamma = rendring.gamma(exr)
        self.visRender(exrgamma)
        lagreRenderBilde = exrgamma
        
    def renderlumkrom(self, exr):
        if(exr.ndim == 3):
            exrlumkrom = rendring.lumkrom(exr)
            self.visRender(exrlumkrom)
            lagreRenderBilde = exrlumkrom
        else:
            self.errormelding.config(text = "Lumkrom fungerer ikke for svart-hvitt-bilder")
            self.errormelding.grid()
            self.errormelding.after(5000, self.fjern_error_melding)
        
    def renderlinspat(self, exr):
        exrlinspat = rendring.linspat(exr)
        self.visRender(exrlinspat)
        lagreRenderBilde = exrlinspat
        
    def renderikkelineaer(self, exr):
        exrlineaer = rendring.ikkelineaer(exr)
        self.visRender(exrlineaer)
        lagreRenderBilde = exrlineaer

    def fjern_error_melding(self):
        self.errormelding.grid_forget()
        
    def lagreBilde(self, renderBilde):
        path = str(filedialog.asksaveasfilename())
        
        if(path):
            if(path[-4] != "."):                         # For å sjekke om bruker tastet .png bak filnavn : Hvis ikke så legg det til
                path = "".join(path + ".png") 
            imageio.imwrite(path, renderBilde)
        else:
            self.errormelding.config(text = "Tomt filnavn, vennligst prøv igjen.")
            self.errormelding.grid()
            self.errormelding.after(5000, self.fjern_error_melding)
    
    def avbrytHDR(self):
        global vb, vh
        vb = 480    # Sett original bredde (som blir overskrevet hvis det er bilder igjen i vinduet)
        vh = 365    # Sett original hoyde
        self.vinduStorrelse()
        self.HDRbilde.grid_remove()
        self.HDRrender_button.grid_remove()
        self.HDRlagre_button.grid_remove()
        self.HDRavbryt_button.grid_remove()
        
    def avbrytRender(self):
        global alleredeBildeHoyre, hb, hh
        hb = 480
        hh = 365
        self.vinduStorrelse()
        self.R_bilde.grid_remove()
        self.RB1log.grid_remove()
        self.RB2gamma.grid_remove()
        self.RB3lumkrom.grid_remove()
        self.RB4linspat.grid_remove()
        self.RB5ikkelineaer.grid_remove()
        self.R_lagre_button.grid_remove()
        self.R_avbryt_button.grid_remove()
        alleredeBildeHoyre = 0



root = Tk()
root.title('HDR rekonstruksjon og rendring')
root.geometry("1000x500+200+200")
root.configure(bg='light gray')
b = GUI(root)
root.mainloop()