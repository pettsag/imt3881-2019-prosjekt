import cv2
import numpy as np

def log(im):
    """
    En rendringsfunksjon som rendrer
    exr ved bruk av den naturlige logaritmen
    
    Parameter
    ---------
    im: bilde/exr-fil, grå/farge (numpy-array float)
    
    Returnerer
    ----------
    hdr*: ferdig rendret bilde (numpy-array float)
    """
    im = im + 0.001          #saa ingen verdier er 0
    hdr = np.log(im)
    hdr = hdr - hdr.min()    #minste tall blir 0
    hdr = hdr / hdr.max()    #største tall 1 (komprimerer)
    return hdr

def gamma(im, y = 0.2):
    """
    En rendringsfunksjon som opphøyer
    bildet med en verdi 
    
    Parameter
    ---------
    im: bilde/exr-fil, grå/farge (numpy-array float)
    y:  float fra 0-1(tall som opphøyes over bildet)
        
    Returnerer
    ----------
    hdr*: ferdig rendret bilde (numpy-array float)
    """
    
    hdr = im - im.min()    #minste tall blir 0
    hdr = hdr**y
    hdr = hdr - hdr.min()
    hdr = hdr / hdr.max()
    return hdr

def lumkrom(im):
    """
    En rendringsfunksjon som deler fargebildet
    i lysheten og fargeinformasjonen og
    rendrer dem sammen
    
    Parameter
    ---------
    im: bilde/exr-fil, farge (numpy-array float)
        
    Returnerer
    ----------
    hdr*: ferdig rendret bilde (numpy-array float)
    """

    hdr = im - im.min() + 0.001 #saa ingen verdier er 0
    Luminans = np.zeros((hdr.shape[0], hdr.shape[1]))
    Kromatisitet = np.zeros((hdr.shape[0], hdr.shape[1]))

    for i in range(hdr.shape[0]):
        for j in range(hdr.shape[1]):
            Luminans[i][j] = hdr[i][j].sum()   #regner luminans R+G+B

    Kromatisitet = hdr/Luminans[:, :, np.newaxis] #regner kromatisitet
    Luminans = np.log(Luminans)                   #rendrer luminans
    hdr = Luminans[:, :, np.newaxis] * Kromatisitet #lager bildet
    hdr = hdr - hdr.min()
    hdr = hdr / hdr.max()
    return hdr

def linspat(im):
    """
    En rendringsfunksjon som skiller
    de små detaljene fra de mer storskala
    endringer i bildet
    
    Parameter
    ---------
    im: bilde/exr-fil, grå/farge (numpy-array float)
        
    Returnerer
    ----------
    hdr*: ferdig rendret bilde (numpy-array float)
    """
    u0 = im - im.min()
    u0 = u0 / u0.max()
    ul = cv2.blur(u0, (3,3))#gjør uskarpt
    ul[ul < 0] = 0
    uh = u0 - ul            #finner detaljer i bildet
    rendr = ul**0.2         #rendrer det uskarpe
    hdr = uh + rendr        #legger tilbake
    hdr = hdr - hdr.min()
    hdr = hdr / hdr.max()
    return hdr

def ikkelineaer(im):
    """
    En rendringsfunksjon veldig lik
    linspat-funksjonen, som reduserer
    skarpe kanter/overganger i bildet
    der det er mye kontrast
    
    Parameter
    ---------
    im: bilde/exr-fil, grå/farge (numpy-array float32)
        
    Returnerer
    ----------
    hdr*: ferdig rendret bilde (numpy-array float32)
    """
    u0 = im - im.min()
    u0 = u0 / u0.max()
    ul = cv2.bilateralFilter(u0, 9, 75, 75)  #gjør uskarpt
    ul[ul < 0] = 0
    uh = u0 - ul
    rendr = ul**0.2
    hdr = uh + rendr
    hdr = hdr - hdr.min()
    hdr = hdr / hdr.max()
    return hdr

