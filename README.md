# Prosjektoppgave for IMT3881 Vitenskapelig programmering, våren 2019

Oppgaveteksten ligger i katalogen `oppgave`. Kildekoden dere lager skal ligge i katalogen `src` og rapporten skal ligge i `rapport`.

Det trengs python og disse bibliotekene trengs for å kjøre koden:
	cv2
	glob
	imageio
	numpy
	PIL.Image, PIL.ImageTk
	
For å kunne lese inn .exr-bilder må man laste ned en imagio plugin:
	"imageio.plugins.freeimage.download()" uten "" i python eller "imageio_download_bin freeimage" uten "" i kommandovinduet
